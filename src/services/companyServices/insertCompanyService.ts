import companyCode from "../../helpers/generateCompanyCode";
import CompanyInterface from "../../interfaces/companyInterface";
import GenderInterface from "../../interfaces/genderInterface";
import IdTypeInterface from "../../interfaces/idTypeInterface";
import UserProfileInterface from "../../interfaces/userProfileInterface";
import modelCompany from "../../models/modelCompany";
import modelGender from "../../models/modelGender";
import modelIdType from "../../models/modelIdType";
const { encriptPassword } = require("../../helpers/helperUser")
const UserProfile = require('../../models/modelUserProfile');
const User = require('../../models/modelUser')
const { configCompany } = require('../../helpers/dataConfig');


const insertCompanyRepository = async (dataCompany: any) => {

    let { user, company } = dataCompany;
    let com_code: string | Error = await companyCode(company.com_name);
    company.com_code = com_code;
    // let {user_name, user_password, pro_name=configCompany.pro_name, ...companyData} = dataCampany

    try {
        if (company.com_nit == "" || company.com_nit == null) {
            console.log("hola")
            delete company.com_nit
        }else{
            console.log(company)
            const getCompany: CompanyInterface = await modelCompany.findOne({ com_nit: company.com_nit })
            console.log("hola")
            if (getCompany) {
                return {
                    status: 608,
                    message: "nit de empresa ya existe"
                }
            }
            
        }
        const insertCompany: CompanyInterface = await modelCompany(company);
        insertCompany.save(async (err: any, product: any, numAffected: number) => {
            console.log("compañia insertada", product)
            if (err) {
                console.log("error al insertar compañia", err)
                return {
                    status: 500,
                    message: err.message
                };
            } else if (product) {

                if (user.user_id == "" || user.user_id == null) {
                    delete user.user_id
                } else {
                    const searchUser = await User.findOne({ user_id: user.user_id });

                    if (searchUser) {
                        return {
                            status: 704,
                            message: "identificacion de usuario ya existe"
                        };
                    }

                }

                const profile: UserProfileInterface = await UserProfile.findOne({
                    pro_name: configCompany.pro_name, pro_status: true
                });
                const idProfile = profile._id;
                if (!idProfile) {
                    return {
                        status: 800,
                        message: "perfil de usuario no existe"
                    }
                }

                const idType: IdTypeInterface = await modelIdType.findOne({ _id: user.user_id_type })

                const gender: GenderInterface = await modelGender.findOne({ _id: user.gender_id })

                user.user_password = encriptPassword(user.user_password);
                user.com_id = product._id;
                user.pro_code = idProfile;
                user.user_id_type = idType?._id;
                user.user_sexo = gender?._id

                const insertUser = await User(user)
                insertUser.save((err: any, user: any) => {
                    console.log("usuario insertado", user)
                    if (err) {
                        return {
                            status: 500,
                            message: err.message
                        };
                    }

                });
            }

        });

        return {
            status: 200,
            message: "usuario de compañia insertado",
            com_code,
        }
    } catch (error: any) {
        console.log(error);
        return {
            status: 500,
            message: error
        }
    }
}

export = insertCompanyRepository
