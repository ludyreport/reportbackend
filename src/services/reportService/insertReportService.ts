

import CategoryInterface from "../../interfaces/categoryInterface";
import ReportInterface from "../../interfaces/reportInterface";
import UserInterface from "../../interfaces/userInterface";
import modelCategory from "../../models/modelCategory";
import modelReport from "../../models/modelReport";
import Report from "../../models/modelReport";
import modelUser from "../../models/modelUser";
const {getAuthUser} = require( "../../middleware/verifyToken");
const {listIds} = require("../../helpers/helperUser");


const insertReportRepository = async(dataReport:any) => {
    const {cat_code, ...data} = dataReport;
    try {
        const {_id, com_id } = getAuthUser();

        const searchCategory:CategoryInterface = await modelCategory.findById({_id:cat_code, cat_status:true});
        if (!searchCategory){
            return {
                status: 803,
                message:"categoria no existe"
            }
        }
        // const searchUsers: UserInterface[] = await modelUser.find({com_id});
        // const idsUser: number = listIds(searchUsers);
        const numberReports: number = await modelReport.find({}).count();
        data.rep_code = numberReports + 1
        data.cat_code = searchCategory?._id;
        data.user_code = _id;
        const report: ReportInterface = new Report(data);
        report.save((error:any) =>{
            if (error){
                return {
                    status:500, message:error.message
                };
            }
        
        });
        return {
            status:200,
            message:"reporte insertado",
            rer_code: data.rep_code
        };
        
    } catch (error) {
        console.log(error);
        return {
            status:500,
            message:error
        }
    }
}

export = insertReportRepository
